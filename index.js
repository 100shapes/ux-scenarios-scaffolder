const csvWriter = require('csv-write-stream')
const fs = require('fs')
const Combinatorics = require('js-combinatorics');
const yaml = require('node-yaml');

const input = yaml.readSync('./config.yml')

const [, ...facets] = Object.keys(input)
const {
	headers
} = input;

const writer = csvWriter({ headers })
const values = facets.reduce((curr, facet) => {
	return curr.concat([input[facet]])
}, [])

const cp = Combinatorics.cartesianProduct(...values)

writer.pipe(fs.createWriteStream('out.csv'))
cp.map(scenario => {
	const arrays = scenario.map(facet => facet.split('|'))
	const result = [].concat.apply([], arrays)
	writer.write(result)
});
writer.end()
