# UX Scenarios Scaffolder

Hello, and welcome.

UX designers aren't always the most scrupulous individuals. We get it: designers are creative; they need to concentrate on the bigger picture.

Here's a tool to help. Introducing the UX Scenarios scaffolder by [100 Shapes – UX &amp; design studio](https://www.100shapes.com/?utm_source=gitlab-100s&utm_medium=post-main-cta&utm_campaign=ux-tools).

## Huh? 🤔

It's often useful to think about scenarios when designing an interface. Scenarios allow the designer to think about how things should change under certain conditions. The problem is that it's quite difficult to manage all the scenarios in your head. Especially if you have lots of variables.

This tool will list out all the possible scenarios for you based on the [cartesian product](https://en.wikipedia.org/wiki/Cartesian_product) of all your variables.


### Example: Recipe site 🍲

Imagine you're creating a recipe site. The site has:

- 3 different types of user accounts: _viewer_, _subscriber_, _contributer_ (some are paid-for)
- A favouriting mechanism on each recipe
- 3 layouts: _small_, _medium_ and _large_
- A privacy setting: _public_, _private_

Anyone building this UI on your behalf needs to understand how you want the UI to look for each of these scenarios. Rather than guess that list, use this Scenarios Scaffolding tool.

Set up your variables like this (see [config.yml](config.yml)). The `headers` is important – it must come first – the rest don't matter:

	headers:
		- account_type
		- revenue
		- favourited
		- layout
		- privacy

	account_type:
		- viewer|0
		- subscriber|5
		- contributer|20

	favourited:
		- favourite
		- not-favourited

	layout:
		- small
		- medium
		- large

	privacy:
		- public
		- private


## Notice the `|`?
That allows you to configure extra rows in your columns. For example, you might not want to multiply every scenario by the "revenue" column but having it separate means you'll be able to filter by it later on.

When you run the project you get a `.csv` file which you can import into excel or Google Sheets to process further. It looks like this:

	account_type,revenue,favourited,layout,privacy
	viewer,0,favourite,small,public
	subscriber,5,favourite,small,public
	contributer,20,favourite,small,public
	...


## How do I run this? 💻

It's a node project. If you don't know what that is, I'm afraid we're not quite ready to cater for you yet. Stay tuned: we'll make a web UI soon.

If you're all set, installation is simple.

### Installation

Clone this repo:

	$ git clone git@gitlab.com:100shapes/ux-scenarios-scaffolder.git

Install the dependencies:

	$ yarn

### Running

Easy: modify `config.yml` to suit your project, then run

	$ yarn start


## Next steps ✍️

- Make a web UI
- Add negation to remove non-sensical scenarios

## Notes

### You probably don't need all scenarios

Bear in mind, this tool just gives you all possible scenarios, but the scenarios might not make sense. In our example, users might have to have a _subscriber_ or _contributer_ account type in order to be able to _favorite_ a recipe.

This isn't a problem, just be sure to clean-up your scenarios after you've run this script.

### `out.csv` gets replaced

Each time you run the script, the output `.csv` file gets replace – don't worry about deleting it when you change your `config.yml`.

## Like this? Want more? 🏆

Come say hi:
[@100shapes](https://twitter.com/100shapes)
